// JavaScript Document
$(function() {
    // Stick the #nav to the top of the window
    var nav = $('#nav_wrap');
    var navHomeY = nav.offset().top;
    var isFixed = false;
    var $w = $(window);
	
	$('nav a').hover(
		function(){			
			$('div',this).addClass('hover');
			},
		function(){
			$('div',this).removeClass('hover'); 
        	}
	);
	
    $w.scroll(function() {
        var scrollTop = $w.scrollTop();
        var shouldBeFixed = scrollTop > navHomeY;
        if (shouldBeFixed && !isFixed) {
            nav.css({
                position: 'fixed',
                top: 0,
                left: nav.offset().left,
                width: nav.width(),
				opacity: 0.9
			});
            isFixed = true;
		}
        else if (!shouldBeFixed && isFixed)
        {
            nav.css({
                position: 'static'
            });
            isFixed = false;
        };
		if (scrollTop < 500) {
			nav.css({
				opacity: 1
			});
			$('li a div').removeClass('activated');
			$('li:nth-child(1) a div').addClass('activated');
		}
		else if (scrollTop > 500 && scrollTop < 1500) {
			$('li a div').removeClass('activated');
			$('li:nth-child(2) a div').addClass('activated');
		}
		else if (scrollTop > 1500 && scrollTop < 2400) {
			$('li a div').removeClass('activated');
			$('li:nth-child(3) a div').addClass('activated');
		}
		else if (scrollTop > 2400) {
			$('li a div').removeClass('activated');
			$('li:nth-child(4) a div').addClass('activated');
		};
    });
});
$('a').click(function(){
	var $w = $(window);
	var scrollTop = $w.scrollTop();
	if (scrollTop < 680) {
		$('html, body').animate({
        	scrollTop: $( $.attr(this, 'href') ).offset().top-180
    	}, 1200, "linear");
	}
	else {
		$('html, body').animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top-70
		}, 1200, "linear");
	};
    return false;
});