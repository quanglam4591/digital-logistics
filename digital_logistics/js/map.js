// JavaScript Document
function initialize() {
        var map_canvas = document.getElementById('map_canvas');
        var map_options = {
          center: new google.maps.LatLng(44.5403, -78.5463),
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
          // Create an array of styles.
  var styles = [
    {
    "stylers": [
      { "saturation": -100 }
    ]
  },{
    "featureType": "road.highway",
    "stylers": [
      { "hue": "#00ff2b" },
      { "saturation": 81 },
      { "lightness": -11 }
    ]
  },{
    "featureType": "road.arterial",
    "stylers": [
      { "saturation": 52 },
      { "lightness": -27 },
      { "hue": "#11ff00" }
    ]
  },{
    "featureType": "poi",
    "elementType": "labels.icon",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "poi",
    "elementType": "labels.text",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "transit.station",
    "stylers": [
      { "visibility": "off" }
    ]
  }
  ];
  

  // Create a new StyledMapType object, passing it the array of styles,
  // as well as the name to be displayed on the map type control.
  var styledMap = new google.maps.StyledMapType(styles,
    {name: "Styled Map"});
	

  // Create a map object, and include the MapTypeId to add
  // to the map type control.
  var myLatlng = new google.maps.LatLng(10.790446, 106.692433);
  var mapOptions = {
    zoom: 18,
    center: myLatlng,
	mapTypeControl: false,
	scrollwheel: false,
    mapTypeControlOptions: {
      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
    }
  };
  var map = new google.maps.Map(map_canvas,
    mapOptions);
	var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
	var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
	icon: iconBase + 'firedept.png',
    title:"Digital Logistics"
});
	
	var contentString = '<div class="map_div">'+'<img src="images/telephone.png" alt="">'+'<p>(+84) 8-824-8384</p>'+'</div>'+
'<div class="map_div">'+'<img src="images/mail.png" alt="">'+'<p>webform@ispartners.net</p>'+'</div>'+
'<div class="map_div">'+'<img src="images/place.png" alt="">'+'<p>90D Thach Thi Thanh St.,Tan Dinh Ward Dist.1 HCMC,Vietnam</p>'+'</div>';

	var infowindow = new google.maps.InfoWindow({
      content: contentString
  });
  
  	google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });
	
	

  //Associate the styled map with the MapTypeId and set it to display.
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');
      }
      google.maps.event.addDomListener(window, 'load', initialize);